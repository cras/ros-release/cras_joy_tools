## cras_joy_tools (noetic) - 1.0.2-1

The packages in the `cras_joy_tools` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic cras_joy_tools` on `Fri, 12 Jan 2024 10:54:57 -0000`

The `cras_joy_tools` package was released.

Version of package(s) in repository `cras_joy_tools`:

- upstream repository: https://github.com/ctu-vras/cras_joy_tools
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/cras_joy_tools.git
- rosdistro version: `1.0.1-1`
- old version: `1.0.1-1`
- new version: `1.0.2-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `1.0.0`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## cras_joy_tools (noetic) - 1.0.1-1

The packages in the `cras_joy_tools` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic --new-track cras_joy_tools` on `Wed, 15 Nov 2023 13:35:23 -0000`

The `cras_joy_tools` package was released.

Version of package(s) in repository `cras_joy_tools`:

- upstream repository: https://github.com/ctu-vras/cras_joy_tools
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `1.0.1-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `1.0.0`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


